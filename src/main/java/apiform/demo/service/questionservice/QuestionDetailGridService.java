package apiform.demo.service.questionservice;


import apiform.demo.entity.questionEntity.QuestionDetailGrid;

import java.util.List;
import java.util.Optional;

public interface QuestionDetailGridService {
    Optional<QuestionDetailGrid> findById(Integer id);
    void deleteQDetailGrid (Integer idQuestion);
    public List<QuestionDetailGrid> getAllByQuestionId(Integer id);
    public List<QuestionDetailGrid> getRowsByQuestionId(Integer id);
    public List<QuestionDetailGrid> getColsByQuestionId(Integer id);
    void saveOptionGridByQuestion(QuestionDetailGrid questionDetailGrid);
    void saveOptions(QuestionDetailGrid questionDetailGrid);
    void delteAllOption(Integer id);
}
