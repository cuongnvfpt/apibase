package apiform.demo.service.questionservice;


import apiform.demo.entity.questionEntity.QuestionDetail;

import java.util.List;
import java.util.Optional;

public interface QuestionDetailService {
    void deleteQDetail (Integer idQuestion);
    List<QuestionDetail> getAllByQuestionId(Integer id);
    void saveOptionsByQuestion(QuestionDetail questionDetail);
    void deleteOptions(Integer id);
    Optional<QuestionDetail> getById(Integer id);
}
