package apiform.demo.service.questionservice.impl;

import apiform.demo.entity.questionEntity.Questions;
import apiform.demo.repository.questionRepository.QuestionRepository;
import apiform.demo.service.questionservice.QuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class QuestionServiceImpl implements QuestionService {
    private QuestionRepository questionRepository;
    @Autowired
    public QuestionServiceImpl (QuestionRepository questionRepository){
        this.questionRepository = questionRepository;
    }

    @Override
    public Optional<Questions> findQbyId(Integer id) {
        return questionRepository.findById(id);
    }

    @Override
    public void removeQ(Integer id) {
        questionRepository.deleteQById(id);
    }

    @Override
    public List<Questions> listQuestionbyIdpart(Integer idpart) {
        return questionRepository.listQuestionbyIdpart(idpart);
    }

    @Override
    public List<Questions> getAllByPartId(Integer id) {
        return questionRepository.findByPartId(id);
    }

    @Override
    public void saveQuestion(Questions question) {
        questionRepository.save(question);
    }

    @Override
    public List<Integer> getIdQuestionByPart(int idPart) {
        return questionRepository.findIdByPartId(idPart);
    }

    @Override
    public void deleteByQuestionId(Integer id) {
        questionRepository.deleteById(id);
    }

    @Override
    public List<Questions> findQbyIdForm(Integer id) {
        return questionRepository.findQbyIdForm(id);
    }
}
