package apiform.demo.service.questionservice.impl;

import apiform.demo.entity.questionEntity.QuestionDetailGrid;
import apiform.demo.repository.questionRepository.QuestionDetailGridRepository;
import apiform.demo.service.questionservice.QuestionDetailGridService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class QuestionDetailGridServiceImpl implements QuestionDetailGridService {
    private QuestionDetailGridRepository questionDetailGridRepository;
    public QuestionDetailGridServiceImpl (QuestionDetailGridRepository questionDetailGridRepository){
        this.questionDetailGridRepository = questionDetailGridRepository;
    }

    @Override
    public Optional<QuestionDetailGrid> findById(Integer id) {
        return questionDetailGridRepository.findById(id);
    }

    @Override
    public void deleteQDetailGrid(Integer idQuestion) {
        questionDetailGridRepository.deleteQDetailGrid(idQuestion);
    }

    @Override
    public List<QuestionDetailGrid> getAllByQuestionId(Integer id) {
        return questionDetailGridRepository.findByQuestionId(id);
    }

    @Override
    public List<QuestionDetailGrid> getRowsByQuestionId(Integer id) {
        return questionDetailGridRepository.findRowsByQuesionId(id);
    }

    @Override
    public List<QuestionDetailGrid> getColsByQuestionId(Integer id) {
         return questionDetailGridRepository.findColsByQuesionId(id);
    }

    @Override
    public void saveOptionGridByQuestion(QuestionDetailGrid questionDetailGrid) {
        questionDetailGridRepository.save(questionDetailGrid);
    }

    @Override
    public void saveOptions(QuestionDetailGrid questionDetailGrid) {
        questionDetailGridRepository.save(questionDetailGrid);
    }

    @Override
    public void delteAllOption(Integer id) {
        questionDetailGridRepository.deleteById(id);
    }
}
