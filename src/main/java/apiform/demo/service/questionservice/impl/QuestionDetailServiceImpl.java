package apiform.demo.service.questionservice.impl;

import apiform.demo.entity.questionEntity.QuestionDetail;
import apiform.demo.repository.questionRepository.QuestionDetailRepository;
import apiform.demo.service.questionservice.QuestionDetailService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class QuestionDetailServiceImpl implements QuestionDetailService {
    private QuestionDetailRepository questionDetailRepository;
    public QuestionDetailServiceImpl (QuestionDetailRepository questionDetailRepository){
        this.questionDetailRepository = questionDetailRepository;
    }
    @Override
    public void deleteQDetail(Integer idQuestion) {
        questionDetailRepository.deleteQDetail(idQuestion);
    }
    @Override
    public List<QuestionDetail> getAllByQuestionId(Integer id) {
        return questionDetailRepository.findOptionsByQuestionId(id);
    }

    @Override
    public void saveOptionsByQuestion(QuestionDetail questionDetail) {
        questionDetailRepository.save(questionDetail);
    }

    @Override
    public void deleteOptions(Integer id) {
        questionDetailRepository.deleteById(id);
    }

    @Override
    public Optional<QuestionDetail> getById(Integer id) {
        return questionDetailRepository.findById(id);
    }
}
