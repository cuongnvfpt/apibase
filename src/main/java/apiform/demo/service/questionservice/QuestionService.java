package apiform.demo.service.questionservice;



import apiform.demo.entity.questionEntity.Questions;

import java.util.List;
import java.util.Optional;

public interface QuestionService {
    Optional <Questions> findQbyId (Integer id);
    void removeQ(Integer id);
    List<Questions> listQuestionbyIdpart(Integer idpart);
    List<Questions> getAllByPartId(Integer id);
    void saveQuestion(Questions question);
    List<Integer> getIdQuestionByPart(int idPart);
    void deleteByQuestionId(Integer id);
    List<Questions> findQbyIdForm(Integer id);
}
