package apiform.demo.service.loginService;


import apiform.demo.entity.loginEntity.User;

import java.util.Optional;

public interface UserService {

    User findByUsername(String username);

    Optional<User> findByEmail(String email);

    Optional<User> findByResettocken(String resettocken);

    public void save(User user);

}
