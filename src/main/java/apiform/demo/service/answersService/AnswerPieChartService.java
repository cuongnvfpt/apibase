package apiform.demo.service.answersService;




import apiform.demo.model.ObjectAnswerPieChart;
import apiform.demo.entity.answersEntity.AnswerPieChart;

import java.util.List;
import java.util.Optional;

public interface AnswerPieChartService {
    Optional<AnswerPieChart> listById(Integer id);
    void save (AnswerPieChart tbl_answerPieChart);
    void deleteAswerPieChart(Integer idanswer);
    List<ObjectAnswerPieChart> listAnswerPieChart(Integer idanswer);
    void deletePieChart(Integer idAnswer);
}
