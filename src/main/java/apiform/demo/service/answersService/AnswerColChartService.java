package apiform.demo.service.answersService;


import apiform.demo.model.ObjectAnswerCol;
import apiform.demo.entity.answersEntity.AnswerColChart;

import java.util.List;
import java.util.Optional;

public interface AnswerColChartService {
    void save(AnswerColChart tbl_answerColChart);
    void deletetCol(Integer idanswer);
    List<AnswerColChart> getListColChart(Integer idAnswer);
    List<String> getListColChartDis(Integer idAnswer);
    List<String> getListRowChartDis(Integer idAnswer);
    ObjectAnswerCol getOb(Integer idAnswer);
    List<Integer> getAnswerIdByForm(Integer idForm);
    Optional<AnswerColChart> getAnswerColChart(String row, String col,Integer idAnswer);
    void deleteRowsColChart(String row, Integer idAnswer);
    void deleteColsColChart(String col, Integer idAnswer);
}
