package apiform.demo.service.answersService.impl;

import apiform.demo.entity.answersEntity.Answers;
import apiform.demo.repository.answerRepository.AnswersRepository;
import apiform.demo.service.answersService.AnswersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class AnswerServiceImpl implements AnswersService {
    private AnswersRepository answersRepository;

    @Autowired
    public AnswerServiceImpl(AnswersRepository answersRepository) {
        this.answersRepository = answersRepository;
    }

    @Override
    public void save(Answers Answers) {
        answersRepository.save(Answers);
    }

    @Override
    public void deletetblAnswers(Integer idquestion) {
        answersRepository.deletetblAnswers(idquestion);
    }

    @Override
    public Optional<Answers> selectByIdQuestion(Integer idquestion) {
        return answersRepository.selectByIdAnswer(idquestion);
    }

    @Override
    public List<Answers> selectByIdForm(Integer idform) {
        return answersRepository.selectByIdForm(idform);
    }


}
