package apiform.demo.service.answersService.impl;

import apiform.demo.model.ObjectAnswerText;
import apiform.demo.entity.answersEntity.AnswersText;
import apiform.demo.repository.answerRepository.AnswersTextRepository;
import apiform.demo.service.answersService.AnswerTextService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class AnswerTextServiceImpl implements AnswerTextService {
    private AnswersTextRepository answersTextRepository;

    @Autowired
    public AnswerTextServiceImpl(AnswersTextRepository answersTextRepository) {
        this.answersTextRepository = answersTextRepository;
    }

    @Override
    public void save(AnswersText AnswersText) {
        answersTextRepository.save(AnswersText);
    }

    @Override
    public void deleteAswerText(Integer idanswer) {
        answersTextRepository.deletetblAnswers(idanswer);
    }

    @Override
    public List<ObjectAnswerText> selectbyIdA(Integer idanswer) {
        List<Object[]> objects = answersTextRepository.selectbyIdA(idanswer);
        List<ObjectAnswerText> answerText = new ArrayList<>();
        for (Object[] item: objects){
            answerText.add(new ObjectAnswerText(item[0].toString(),Integer.parseInt(item[1].toString()),Integer.parseInt(item[2].toString())));
        }
        return answerText;
    }
}
