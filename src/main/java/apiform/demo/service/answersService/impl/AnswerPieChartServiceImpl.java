package apiform.demo.service.answersService.impl;

import apiform.demo.model.ObjectAnswerPieChart;
import apiform.demo.entity.answersEntity.AnswerPieChart;
import apiform.demo.repository.answerRepository.AnswerPieChartRepository;
import apiform.demo.service.answersService.AnswerPieChartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class AnswerPieChartServiceImpl implements AnswerPieChartService {
    private AnswerPieChartRepository answerPieChartRepository;

    @Autowired
    public AnswerPieChartServiceImpl(AnswerPieChartRepository answerPieChartRepository) {
        this.answerPieChartRepository = answerPieChartRepository;
    }

    @Override
    public Optional<AnswerPieChart> listById(Integer id) {
        return answerPieChartRepository.findById(id);
    }

    @Override
    public void save(AnswerPieChart tbl_answerPieChart) {
        answerPieChartRepository.save(tbl_answerPieChart);
    }

    @Override
    public void deleteAswerPieChart(Integer idanswer) {
        answerPieChartRepository.deletetblAnswers(idanswer);
    }

    @Override
    public List<ObjectAnswerPieChart> listAnswerPieChart(Integer idanswer) {
        List<Object[]> objects = answerPieChartRepository.listAnswerPieChart(idanswer);
        List<ObjectAnswerPieChart> answerTextDTOS = new ArrayList<>();
        for (Object[] item: objects){
            answerTextDTOS.add(new ObjectAnswerPieChart(item[0].toString(),Integer.parseInt(item[1].toString())));
        }
         return answerTextDTOS;
    }

    @Override
    public void deletePieChart(Integer idAnswer) {
        answerPieChartRepository.deletePieChart(idAnswer);
    }
}
