package apiform.demo.service.answersService.impl;

import apiform.demo.model.ObjectAnswerCol;
import apiform.demo.entity.answersEntity.AnswerColChart;
import apiform.demo.repository.answerRepository.AnswerColChartRepository;
import apiform.demo.service.answersService.AnswerColChartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class AnswerColChartServiceImpl implements AnswerColChartService {
    private AnswerColChartRepository answerColChartRepository;

    @Autowired
    public AnswerColChartServiceImpl(AnswerColChartRepository answerColChartRepository) {
        this.answerColChartRepository = answerColChartRepository;
    }

    @Override
    public void save(AnswerColChart tbl_answerColChart) {
        answerColChartRepository.save(tbl_answerColChart);

    }

    @Override
    public void deletetCol(Integer idanswer) {
        answerColChartRepository.deletetCol(idanswer);
    }

    @Override
    public List<AnswerColChart> getListColChart(Integer idAnswer) {
        return answerColChartRepository.getListColChart(idAnswer);
    }

    @Override
    public List<String> getListColChartDis(Integer idAnswer) {
        List<String> stringCol = answerColChartRepository.getListColChartDis(idAnswer);

        return stringCol;
    }

    @Override
    public List<String> getListRowChartDis(Integer idAnswer) {
        List<String> objects = answerColChartRepository.getListRowChartDis(idAnswer);
        return objects;
    }

    @Override
    public ObjectAnswerCol getOb(Integer idAnswer) {
        ObjectAnswerCol objectAnswerCol = new ObjectAnswerCol();
        objectAnswerCol.setListCol(getListColChartDis(idAnswer));
        objectAnswerCol.setListColChart(getListColChart(idAnswer));
        objectAnswerCol.setListRow(getListRowChartDis(idAnswer));
        return objectAnswerCol;
    }

    @Override
    public List<Integer> getAnswerIdByForm(Integer idForm) {
        return answerColChartRepository.getAnswerIdByForm(idForm);
    }

    @Override
    public Optional<AnswerColChart> getAnswerColChart(String row, String col, Integer idAnswer) {
        return answerColChartRepository.getAnswerColChart(row, col,idAnswer);
    }

    @Override
    public void deleteRowsColChart(String row, Integer idAnswer) {
        answerColChartRepository.deleteRowsColChart(row, idAnswer);
    }

    @Override
    public void deleteColsColChart(String col, Integer idAnswer) {
        answerColChartRepository.deleteColsColChart(col, idAnswer);
    }

}
