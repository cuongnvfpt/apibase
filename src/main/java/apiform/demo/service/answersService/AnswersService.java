package apiform.demo.service.answersService;



import apiform.demo.entity.answersEntity.Answers;

import java.util.List;
import java.util.Optional;

public interface AnswersService {
    void save(Answers Answers);
    void deletetblAnswers(Integer idquestion);
    Optional<Answers> selectByIdQuestion(Integer idquestion);
    List<Answers> selectByIdForm(Integer idform);
}
