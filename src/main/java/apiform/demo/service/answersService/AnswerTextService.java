package apiform.demo.service.answersService;


import apiform.demo.model.ObjectAnswerText;
import apiform.demo.entity.answersEntity.AnswersText;

import java.util.List;


public interface AnswerTextService {
    void save (AnswersText AnswersText);
    void deleteAswerText(Integer idanswer);
    List<ObjectAnswerText> selectbyIdA(Integer idanswer);
}
