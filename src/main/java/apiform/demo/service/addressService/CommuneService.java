package apiform.demo.service.addressService;



import apiform.demo.entity.addressEntity.Commune;

import java.util.List;

public interface CommuneService {
    public List<Commune> getListCommune(String id);
}
