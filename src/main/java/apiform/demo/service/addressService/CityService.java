package apiform.demo.service.addressService;



import apiform.demo.entity.addressEntity.City;

import java.util.List;

public interface CityService {
    public List<City> listCity();
}
