package apiform.demo.service.addressService;


import apiform.demo.entity.addressEntity.District;

import java.util.List;

public interface DistrictService {
    public List<District> getListDistrict(String id);
}
