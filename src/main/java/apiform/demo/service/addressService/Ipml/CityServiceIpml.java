package apiform.demo.service.addressService.Ipml;

import apiform.demo.entity.addressEntity.City;
import apiform.demo.repository.addressRepository.CityRespository;
import apiform.demo.service.addressService.CityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CityServiceIpml implements CityService {

    @Autowired
    CityRespository cityRespository;

    @Override
    public List<City> listCity() {
        return cityRespository.listCity();
    }
}
