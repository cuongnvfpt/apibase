package apiform.demo.service.addressService.Ipml;

import apiform.demo.entity.addressEntity.District;
import apiform.demo.repository.addressRepository.DistrictRespository;
import apiform.demo.service.addressService.DistrictService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DistrictServiceIpml implements DistrictService {

    @Autowired
    DistrictRespository districtRespository;

    public List<District> getListDistrict(String id){
        return districtRespository.getListDistrict(id);
    }
}
