package apiform.demo.service.addressService.Ipml;

import apiform.demo.entity.addressEntity.Commune;
import apiform.demo.repository.addressRepository.CommuneRespository;
import apiform.demo.service.addressService.CommuneService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CommuneServiceIpml implements CommuneService {

    @Autowired
    CommuneRespository communeRespository;

    @Override
    public List<Commune> getListCommune(String id) {
        return communeRespository.getCommune(id);
    }
}
