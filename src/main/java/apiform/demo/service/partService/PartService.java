package apiform.demo.service.partService;




import apiform.demo.entity.partEntity.Part;

import java.util.List;
import java.util.Optional;

public interface PartService {
    Optional<Part> findById(Integer id);
    List<Part> getlistpart(Integer idForm);
    void deletePartbyIdForm(Integer idForm);
    List<Part> getAllByFormId(Integer id);
    void savePart(Part part);
    List<Integer> getIdPartByLastForm(Integer idLastForm);
    void deletePartById(Integer id);
}
