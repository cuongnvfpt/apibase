package apiform.demo.service.partService.impl;

import apiform.demo.entity.partEntity.Part;
import apiform.demo.repository.partRepository.PartRepository;
import apiform.demo.service.partService.PartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PartServiceImpl implements PartService {
    private PartRepository partRepository;
    @Autowired
    public PartServiceImpl(PartRepository partRepository){
        this.partRepository = partRepository;
    }

    @Override
    public Optional<Part> findById(Integer id) {
        return partRepository.findById(id);
    }

    @Override
    public List<Part> getlistpart(Integer idForm) {
        return partRepository.getlistpart(idForm);
    }

    @Override
    public void deletePartbyIdForm(Integer idForm) {
        partRepository.deletePartbyIdForm(idForm);
    }
    @Override
    public List<Part> getAllByFormId(Integer id) {
        return partRepository.findByFormId(id);
    }

    @Override
    public void savePart(Part part) {
        partRepository.save(part);
    }

    @Override
    public List<Integer> getIdPartByLastForm(Integer idLastForm) {
        return partRepository.findIdLastPart(idLastForm);
    }

    @Override
    public void deletePartById(Integer id) {
        partRepository.deleteById(id);
    }


}
