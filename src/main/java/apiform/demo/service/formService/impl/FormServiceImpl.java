package apiform.demo.service.formService.impl;

import apiform.demo.entity.formEntity.Form;
import apiform.demo.repository.formRepository.FormRespository;
import apiform.demo.service.formService.FormService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class FormServiceImpl implements FormService {
    private FormRespository formRespository;
    @Autowired
    public FormServiceImpl (FormRespository formRespository){
        this.formRespository = formRespository;
    }

    @Override
    public List<Form> getAllForm(Integer iduser, int offset, int limit) {
        List<Form> f = formRespository.finfAllForm(iduser, PageRequest.of(offset,limit, Sort.by("id").descending()));
        return f;
    }

    @Override
    public List<Form> getAllFormAZ(Integer iduser, int offset, int limit) {
        List<Form> f = formRespository.finfAllForm(iduser, PageRequest.of(offset,limit, Sort.by("title").ascending()));
        return f;
    }

    @Override
    public List<Form> getAllFormZA(Integer iduser, int offset, int limit) {
        List<Form> f = formRespository.finfAllForm(iduser, PageRequest.of(offset,limit, Sort.by("title").descending()));
        return f;
    }

    @Override
    public void saveForm(Form form) {
        formRespository.save(form);
    }

    @Override
    public Optional<Form> findByIdForm(Integer id) {
        return formRespository.findById(id);
    }

    @Override
    public void removeForm(Form form) {
        formRespository.delete(form);
    }


    @Override
    public Optional<Integer> idLastForm() {
        return formRespository.findIdLastForm();
    }

    @Override
    public List<Form> finfAllFormbyTitle(String title,Integer idUser, int offset, int limit) {
        List<Form> f = formRespository.finfAllFormbyTitle(title,idUser,PageRequest.of(offset,limit, Sort.by("id").descending()));
        return f;
    }

    @Override
    public Integer countFormsbyTitle(String title,Integer idUser) {
        return formRespository.countFormsbyTitle(title,idUser);
    }

    @Override
    public Form findbyIdFormUser(Integer id, Integer idUser) {
        return formRespository.findbyIdForm(id,idUser);
    }


}
