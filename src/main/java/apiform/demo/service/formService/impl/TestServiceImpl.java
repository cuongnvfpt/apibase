package apiform.demo.service.formService.impl;

import apiform.demo.model.ObjectAnswerPieChart;
import apiform.demo.model.ObjectAnswerText;
import apiform.demo.model.Test;
import apiform.demo.repository.answerRepository.AnswerPieChartRepository;
import apiform.demo.repository.answerRepository.AnswersTextRepository;
import apiform.demo.service.formService.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class TestServiceImpl implements TestService {
    private AnswersTextRepository answersTextRepository;
    private AnswerPieChartRepository answerPieChartRepository;

    @Autowired
    public TestServiceImpl(AnswersTextRepository answersTextRepository, AnswerPieChartRepository answerPieChartRepository) {
        this.answersTextRepository = answersTextRepository;
        this.answerPieChartRepository = answerPieChartRepository;
    }

    @Override
    public Test test(Integer idanswer) {
        Test test = new Test();
        List<Object[]> objects = answersTextRepository.selectbyIdA(idanswer);
        List<ObjectAnswerText> answerText = new ArrayList<>();
        for (Object[] item: objects){
            answerText.add(new ObjectAnswerText(item[0].toString(),Integer.parseInt(item[1].toString()),Integer.parseInt(item[2].toString())));
        }
        List<Object[]> objectss = answerPieChartRepository.listAnswerPieChart(idanswer);
        List<ObjectAnswerPieChart> answerTextDTOS = new ArrayList<>();
        for (Object[] item: objectss){
            answerTextDTOS.add(new ObjectAnswerPieChart(item[0].toString(),Integer.parseInt(item[1].toString())));
        }
        test.setListText(answerText);
        test.setListPie(answerTextDTOS);
        return test;
    }
}
