package apiform.demo.service.formService;



import apiform.demo.entity.formEntity.Form;

import java.util.List;
import java.util.Optional;

public interface FormService {
    List<Form> getAllForm(Integer iduser, int offset, int limit);
    List<Form> getAllFormAZ(Integer iduser, int offset, int limit);
    List<Form> getAllFormZA(Integer iduser, int offset, int limit);
    void saveForm(Form form);
    Optional <Form> findByIdForm(Integer id);
    void removeForm(Form form);
    Optional<Integer> idLastForm();
    List<Form> finfAllFormbyTitle(String title,Integer idUser, int offset, int limit);
    Integer countFormsbyTitle(String title,Integer idUser);
    Form findbyIdFormUser(Integer id,Integer idUser);
}
