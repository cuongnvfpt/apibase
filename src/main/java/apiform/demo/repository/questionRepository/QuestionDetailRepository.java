package apiform.demo.repository.questionRepository;

import apiform.demo.entity.questionEntity.QuestionDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import javax.transaction.Transactional;
import java.util.List;

public interface QuestionDetailRepository extends JpaRepository<QuestionDetail,Integer> {
    @Transactional
    @Modifying
    @Query("DELETE FROM QuestionDetail qd WHERE qd.idQuestion = ?1")
    public void deleteQDetail (Integer idQuestion);
    @Query("SELECT qd FROM QuestionDetail qd where qd.idQuestion = ?1")
    public List<QuestionDetail> findOptionsByQuestionId(Integer id);
}
