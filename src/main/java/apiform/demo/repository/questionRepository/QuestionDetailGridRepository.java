package apiform.demo.repository.questionRepository;

import apiform.demo.entity.questionEntity.QuestionDetailGrid;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import javax.transaction.Transactional;
import java.util.List;

public interface QuestionDetailGridRepository extends JpaRepository<QuestionDetailGrid,Integer> {
    @Transactional
    @Modifying
    @Query("DELETE FROM QuestionDetailGrid qd WHERE qd.idQuestion = ?1")
    public void deleteQDetailGrid (Integer idQuestion);

    @Query("SELECT pdg FROM QuestionDetailGrid pdg where pdg.idQuestion = ?1")
    public List<QuestionDetailGrid> findByQuestionId(Integer id);

    @Query("SELECT qdg FROM QuestionDetailGrid qdg where qdg.idQuestion = ?1 and qdg.rows not like '-1'")
    public List<QuestionDetailGrid> findRowsByQuesionId(Integer id);

    @Query("SELECT qdg FROM QuestionDetailGrid qdg where qdg.idQuestion = ?1 and qdg.cols not like '-1'")
    public List<QuestionDetailGrid> findColsByQuesionId(Integer id);

    @Query("UPDATE QuestionDetailGrid qdg SET qdg.rows = '-1' WHERE qdg.id = ?1")
    public void deleteRowsByQuestionId(Integer id);

    @Query("UPDATE QuestionDetailGrid qdg SET qdg.cols = '-1' WHERE qdg.id = ?1")
    public void deleteColsByQuestionId(Integer id);
}
