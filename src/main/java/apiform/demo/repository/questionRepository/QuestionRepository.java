package apiform.demo.repository.questionRepository;

import apiform.demo.entity.questionEntity.Questions;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import javax.transaction.Transactional;
import java.util.List;

public interface QuestionRepository extends JpaRepository<Questions,Integer> {
    @Query("SELECT q FROM Questions q WHERE q.idPart = ?1")
    public List<Questions> listQuestionbyIdpart(Integer idpart);
    @Transactional
    @Modifying
    @Query("DELETE FROM Questions q WHERE q.idPart = ?1")
    public void deleteQById(Integer idpart);
    @Query("SELECT q FROM Questions q where q.idPart = ?1")
    public List<Questions> findByPartId(Integer id);
    @Query("SELECT q.id FROM Questions q where q.idPart = ?1")
    public List<Integer> findIdByPartId(Integer id);
    @Query("SELECT   NEW Questions ( q.id , q.question,q.image)\n" +
            "FROM            Form f INNER JOIN\n" +
            "                         Part p ON f.id = p.idForm INNER JOIN\n" +
            "                         Questions q ON p.id = q.idPart\n" +
            " Where f.id = ?1")
    public List<Questions> findQbyIdForm(Integer id);

}
