package apiform.demo.repository.addressRepository;


import apiform.demo.entity.addressEntity.Commune;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CommuneRespository extends JpaRepository<Commune,String> {
    @Query("SELECT new Commune( c.idPx, c.communeName , c.idQh ) FROM Commune c where c.idQh like ?1 ")
    public List<Commune> getCommune(@Param("idQh") String idQh);
}
