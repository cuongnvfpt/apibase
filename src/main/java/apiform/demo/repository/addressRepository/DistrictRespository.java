package apiform.demo.repository.addressRepository;

import apiform.demo.entity.addressEntity.District;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DistrictRespository extends JpaRepository<District,String> {
    @Query("SELECT distinct new District( d.idDistrict ,d.districtName, d.idCity ) FROM District d where d.idCity like ?1 ")
    public List<District> getListDistrict(@Param("idCity") String id);
}
