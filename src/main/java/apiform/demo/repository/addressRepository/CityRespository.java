package apiform.demo.repository.addressRepository;


import apiform.demo.entity.addressEntity.City;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CityRespository extends JpaRepository<City,String> {
    @Query("SELECT distinct new City( e.idCity , e.city ) FROM City e")
    public List<City> listCity();
}
