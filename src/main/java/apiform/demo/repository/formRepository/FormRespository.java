package apiform.demo.repository.formRepository;


import apiform.demo.entity.formEntity.Form;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Repository
@Transactional
public interface FormRespository extends JpaRepository<Form,Integer> {
    @Query("SELECT f FROM Form f WHERE f.idUser = ?1")
    public List<Form> finfAllForm(Integer iduser, PageRequest page);
    @Query("SELECT MAX(f.id) FROM Form f")
    public Optional<Integer> findIdLastForm();
    @Query("SELECT f FROM Form f WHERE f.title LIKE %:title% and f.idUser=:idUser")
    public List<Form> finfAllFormbyTitle(@Param("title") String title,@Param("idUser") Integer idUser, Pageable page);
    @Query("SELECT count (f) from Form f WHERE f.title LIKE %:title% AND f.idUser=:idUser")
    public Integer countFormsbyTitle(@Param("title") String title, @Param("idUser") Integer idUser);
    @Query("SELECT f FROM Form f WHERE f.id =:id AND f.idUser =:idUser")
    public Form findbyIdForm(@Param("id") Integer id, @Param("idUser") Integer idUser);
}
