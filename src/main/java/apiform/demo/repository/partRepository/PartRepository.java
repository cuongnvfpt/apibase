package apiform.demo.repository.partRepository;

import apiform.demo.entity.partEntity.Part;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import javax.transaction.Transactional;
import java.util.List;

public interface PartRepository extends JpaRepository<Part,Integer> {
    @Query("SELECT p FROM Part p WHERE p.idForm = ?1")
    public List<Part> getlistpart(Integer idForm);
    @Transactional
    @Modifying
    @Query("DELETE FROM Part p WHERE p.idForm = ?1")
    public void deletePartbyIdForm(Integer idForm);

    @Query("SELECT p FROM Part p WHERE p.idForm = ?1")
    public List<Part> findByFormId(Integer idForm);
    @Query("SELECT p.id FROM Part p where p.idForm = ?1")
    public List<Integer> findIdLastPart(Integer idLastForm);

}
