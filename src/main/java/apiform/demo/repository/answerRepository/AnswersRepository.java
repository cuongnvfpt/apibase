package apiform.demo.repository.answerRepository;

import apiform.demo.entity.answersEntity.Answers;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Repository
public interface AnswersRepository extends JpaRepository<Answers,Integer> {
    @Transactional
    @Modifying
    @Query("DELETE FROM Answers a WHERE a.idquestion = ?1")
    public void deletetblAnswers(Integer idquestion);
    @Query("SELECT a FROM Answers a WHERE a.idquestion = ?1")
    public Optional<Answers> selectByIdAnswer(Integer idquestion);
    @Query("SELECT a FROM Answers a WHERE a.idform = ?1")
    public List<Answers> selectByIdForm(Integer idform);
}
