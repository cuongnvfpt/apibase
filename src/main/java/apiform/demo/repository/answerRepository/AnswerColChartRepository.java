package apiform.demo.repository.answerRepository;

import apiform.demo.entity.answersEntity.AnswerColChart;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Repository
public interface AnswerColChartRepository extends JpaRepository<AnswerColChart, Integer> {
    @Transactional
    @Modifying
    @Query("DELETE FROM AnswerColChart a WHERE a.idAnswer = ?1")
    public void deletetCol(Integer idanswer);

    @Query("SELECT a FROM AnswerColChart a WHERE a.idAnswer = ?1 order by a.row asc")
    public List<AnswerColChart> getListColChart(Integer idAnswer);

    @Query("SELECT DISTINCT a.col FROM AnswerColChart a WHERE a.idAnswer = ?1")
    public List<String> getListColChartDis(Integer idAnswer);

    @Query("SELECT DISTINCT a.row FROM AnswerColChart a WHERE a.idAnswer = ?1")
    public List<String> getListRowChartDis(Integer idAnswer);

    @Query("SELECT distinct acc.idAnswer FROM AnswerColChart acc \n" +
            "inner join Answers ans on acc.idAnswer = ans.id \n" +
            "where ans.idform = ?1")
    public List<Integer> getAnswerIdByForm(Integer idForm);

    @Query( nativeQuery = true, value = "SELECT top 1 * FROM tbl_answercolchart acc WHERE acc.row like %:rowS% AND acc.col like %:colS% AND acc.idAnswer = :idAnswer")
    public Optional<AnswerColChart> getAnswerColChart(@Param("rowS") String rowS, @Param("colS") String colS, @Param("idAnswer") Integer idAnswer);

    @Transactional
    @Modifying
    @Query("DELETE FROM AnswerColChart acc WHERE acc.idAnswer = :idAnswer and acc.row like %:row% ")
    public void deleteRowsColChart(String row, Integer idAnswer);

    @Transactional
    @Modifying
    @Query("DELETE FROM AnswerColChart acc WHERE acc.idAnswer = :idAnswer and acc.col like %:col% ")
    public void deleteColsColChart(String col, Integer idAnswer);
}
