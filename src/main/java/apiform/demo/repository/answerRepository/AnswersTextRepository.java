package apiform.demo.repository.answerRepository;

import apiform.demo.entity.answersEntity.AnswersText;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
public interface AnswersTextRepository extends JpaRepository<AnswersText,Integer> {
    @Transactional
    @Modifying
    @Query("DELETE FROM AnswersText a WHERE a.idanswer = ?1")
    public void deletetblAnswers(Integer idanswer);
    @Query("SELECT DISTINCT a.answer,  count(a.answer) as countanswer,a.idanswer FROM AnswersText a WHERE a.idanswer = ?1 GROUP BY a.answer,a.idanswer")
    public List<Object[]> selectbyIdA(Integer idanswer);

}

