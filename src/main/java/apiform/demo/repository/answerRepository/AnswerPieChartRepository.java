package apiform.demo.repository.answerRepository;

import apiform.demo.entity.answersEntity.AnswerPieChart;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
public interface AnswerPieChartRepository extends JpaRepository<AnswerPieChart,Integer> {
    @Transactional
    @Modifying
    @Query("DELETE FROM AnswerPieChart a WHERE a.idanswer = ?1")
    public void deletetblAnswers(Integer idanswer);
    @Query("SELECT DISTINCT a.answer, count(a.answer) as countanswer FROM AnswerPieChart a WHERE a.idanswer = ?1 GROUP BY a.answer")
    public List<Object[]> listAnswerPieChart(Integer idanswer);
    @Transactional
    @Modifying
    @Query("DELETE FROM AnswerPieChart apc WHERE apc.idanswer = ?1")
    public void deletePieChart(Integer idAnswer);
}
