package apiform.demo.controller.formAPI;

import apiform.demo.entity.answersEntity.Answers;
import apiform.demo.entity.partEntity.Part;
import apiform.demo.entity.questionEntity.QuestionDetail;
import apiform.demo.entity.questionEntity.QuestionDetailGrid;
import apiform.demo.entity.questionEntity.Questions;
import apiform.demo.service.answersService.AnswerColChartService;
import apiform.demo.service.answersService.AnswerPieChartService;
import apiform.demo.service.answersService.AnswerTextService;
import apiform.demo.service.answersService.AnswersService;
import apiform.demo.service.formService.FormService;
import apiform.demo.service.partService.PartService;
import apiform.demo.service.questionservice.QuestionDetailGridService;
import apiform.demo.service.questionservice.QuestionDetailService;
import apiform.demo.service.questionservice.QuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(path = "/formAPI")
public class PartController {
    private PartService partService;
    private FormService formService;
    private QuestionService questionService;
    private QuestionDetailService questionDetailService;
    private QuestionDetailGridService questionDetailGridService;
    private AnswersService answersService;
    private AnswerTextService answerTextService;
    private AnswerPieChartService answerPieChartService;
    private AnswerColChartService answerColChartService;

    @Autowired
    public PartController(PartService partService, FormService formService, QuestionService questionService, QuestionDetailService questionDetailService, QuestionDetailGridService questionDetailGridService, AnswersService answersService, AnswerTextService answerTextService, AnswerPieChartService answerPieChartService, AnswerColChartService answerColChartService) {
        this.partService = partService;
        this.formService = formService;
        this.questionService = questionService;
        this.questionDetailService = questionDetailService;
        this.questionDetailGridService = questionDetailGridService;
        this.answersService = answersService;
        this.answerTextService = answerTextService;
        this.answerPieChartService = answerPieChartService;
        this.answerColChartService = answerColChartService;
    }

    @GetMapping("/forms/part/{id}")
    public ResponseEntity<List<Part>> getPartByFormId(@PathVariable("id") Integer id) {
        List<Part> parts = partService.getAllByFormId(id);
        if (parts.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(parts, HttpStatus.OK);
    }

    @PostMapping("/forms/part/insert")
    public ResponseEntity<Part> insertPartByFormId(@RequestBody Part part) {
//        part.setIdForm(formService.idLastForm());
        partService.savePart(part);
        return new ResponseEntity<>(part, HttpStatus.CREATED);
    }

    @GetMapping("/forms/part/lastId/{id}")
    public List<Integer> getIdLastPart(@PathVariable("id") Integer id){
        List<Integer> idPart = partService.getIdPartByLastForm(id);
        return idPart;
    }

    @DeleteMapping("/forms/part/delete/{id}")
    public ResponseEntity<Part> deletePart(@PathVariable(name = "id") Integer id) {
        Optional<Part> p = partService.findById(id);
        if (!p.isPresent()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } else {
            //delete question by part form
            List<Questions> questions = questionService.getAllByPartId(p.get().getId());
            if (!questions.isEmpty()) {
                for (Questions q : questions) {
                    List<QuestionDetail> questionDetails = questionDetailService.getAllByQuestionId(q.getId());
                    List<QuestionDetailGrid> questionDetailGrids = questionDetailGridService.getAllByQuestionId(q.getId());
                    Optional<Answers> answer = answersService.selectByIdQuestion(q.getId());

                    //delete all options by question id
//                    if (questionDetails.isEmpty() && questionDetailGrids.isEmpty() && !answer.isPresent()) {
//                        questionService.deleteByQuestionId(q.getId());
//                    }
                    if (!questionDetails.isEmpty()) {
                        for (QuestionDetail questionDetail : questionDetails) {
                            questionDetailService.deleteOptions(questionDetail.getId());
                        }
                    }
                    if (!questionDetailGrids.isEmpty()) {
                        for (QuestionDetailGrid questionDetailGrid : questionDetailGrids) {
                            questionDetailGridService.delteAllOption(questionDetailGrid.getId());
                        }
                    }
                    if (answer.isPresent()) {
                        answerTextService.deleteAswerText(answer.get().getId());
                        answerPieChartService.deleteAswerPieChart(answer.get().getId());
                        answerColChartService.deletetCol(answer.get().getId());
                        answersService.deletetblAnswers(q.getId());
                    }

                    questionService.deleteByQuestionId(q.getId());

                }
                partService.deletePartById(p.get().getId());
                return new ResponseEntity<>(HttpStatus.OK);
            }
            partService.deletePartById(p.get().getId());
            return new ResponseEntity<>(HttpStatus.OK);
        }

    }
}
