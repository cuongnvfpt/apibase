package apiform.demo.controller.formAPI;

import apiform.demo.entity.answersEntity.Answers;
import apiform.demo.entity.questionEntity.QuestionDetail;
import apiform.demo.entity.questionEntity.QuestionDetailGrid;
import apiform.demo.entity.questionEntity.Questions;
import apiform.demo.service.answersService.AnswerColChartService;
import apiform.demo.service.answersService.AnswerPieChartService;
import apiform.demo.service.answersService.AnswerTextService;
import apiform.demo.service.answersService.AnswersService;
import apiform.demo.service.questionservice.QuestionDetailGridService;
import apiform.demo.service.questionservice.QuestionDetailService;
import apiform.demo.service.questionservice.QuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(path = "/formAPI")
public class QuestionController {
    private QuestionService questionService;
    private QuestionDetailService questionDetailService;
    private QuestionDetailGridService questionDetailGridService;
    private AnswersService answersService;
    private AnswerTextService answerTextService;
    private AnswerPieChartService answerPieChartService;
    private AnswerColChartService answerColChartService;

    @Autowired
    public QuestionController(QuestionService questionService, QuestionDetailService questionDetailService, QuestionDetailGridService questionDetailGridService, AnswersService answersService, AnswerTextService answerTextService, AnswerPieChartService answerPieChartService, AnswerColChartService answerColChartService) {
        this.questionService = questionService;
        this.questionDetailService = questionDetailService;
        this.questionDetailGridService = questionDetailGridService;
        this.answersService = answersService;
        this.answerTextService = answerTextService;
        this.answerPieChartService = answerPieChartService;
        this.answerColChartService = answerColChartService;
    }

    @GetMapping("/forms/part/questions/{id}")
    public ResponseEntity<List<Questions>> getQuestionByPartId(@PathVariable("id") Integer id) {
        List<Questions> questions = questionService.getAllByPartId(id);
        if (questions.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(questions, HttpStatus.OK);
    }



    @PostMapping("/forms/question/insert")
    public ResponseEntity<Questions> insertQuestionByPartId(@RequestBody Questions questions) {
        questionService.saveQuestion(questions);
        return new ResponseEntity<>(questions, HttpStatus.CREATED);
    }
    @GetMapping("/forms/question/partId/{id}")
    public List<Integer> getListIdByPart(@PathVariable("id") Integer id){
        List<Integer> idQuestion = questionService.getIdQuestionByPart(id);
        return idQuestion;
    }

    @DeleteMapping("/forms/question/delete/{id}")
    public ResponseEntity<Questions> deleteQuestion(@PathVariable(name = "id") Integer id) {
        Optional<Questions> q = questionService.findQbyId(id);
        if (!q.isPresent()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } else {
            List<QuestionDetail> questionDetails = questionDetailService.getAllByQuestionId(q.get().getId());
            List<QuestionDetailGrid> questionDetailGrids = questionDetailGridService.getAllByQuestionId(q.get().getId());
            Optional<Answers> answer = answersService.selectByIdQuestion(q.get().getId());

            //delete all options by question id
            if (!questionDetails.isEmpty()) {
                for (QuestionDetail questionDetail : questionDetails) {
                    questionDetailService.deleteOptions(questionDetail.getId());
                }
            }
            if (!questionDetailGrids.isEmpty()) {
                for (QuestionDetailGrid questionDetailGrid : questionDetailGrids) {
                    questionDetailGridService.delteAllOption(questionDetailGrid.getId());
                }
            }
            if (answer.isPresent()) {
                answerTextService.deleteAswerText(answer.get().getId());
                answerPieChartService.deleteAswerPieChart(answer.get().getId());
                answerColChartService.deletetCol(answer.get().getId());
                answersService.deletetblAnswers(q.get().getId());
            }
        }
        questionService.deleteByQuestionId(q.get().getId());
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
