package apiform.demo.controller.formAPI;

import apiform.demo.entity.answersEntity.Answers;
import apiform.demo.entity.formEntity.Form;
import apiform.demo.entity.loginEntity.User;
import apiform.demo.entity.partEntity.Part;
import apiform.demo.entity.questionEntity.Questions;
import apiform.demo.service.answersService.AnswerColChartService;
import apiform.demo.service.answersService.AnswerPieChartService;
import apiform.demo.service.answersService.AnswerTextService;
import apiform.demo.service.answersService.AnswersService;
import apiform.demo.service.formService.FormService;
import apiform.demo.service.loginService.CustomUserDetail;
import apiform.demo.service.loginService.UserService;
import apiform.demo.service.partService.PartService;
import apiform.demo.service.questionservice.QuestionDetailGridService;
import apiform.demo.service.questionservice.QuestionDetailService;
import apiform.demo.service.questionservice.QuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.sql.Date;
import java.util.Calendar;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(path = "/formAPI")
public class FormController {
    private FormService formService;
    private PartService partService;
    private QuestionDetailService questionDetailService;
    private QuestionService questionService;
    private QuestionDetailGridService questionDetailGridService;
    private AnswersService answersService;
    private AnswerTextService answerTextService;
    private AnswerPieChartService answerPieChartService;
    private AnswerColChartService answerColChartService;
    private UserService userService;

    @Autowired
    public FormController(FormService formService, PartService partService, QuestionDetailService questionDetailService, QuestionService questionService, QuestionDetailGridService questionDetailGridService, AnswersService answersService, AnswerTextService answerTextService, AnswerPieChartService answerPieChartService, AnswerColChartService answerColChartService, UserService userService) {
        this.formService = formService;
        this.partService = partService;
        this.questionDetailService = questionDetailService;
        this.questionService = questionService;
        this.questionDetailGridService = questionDetailGridService;
        this.answersService = answersService;
        this.answerTextService = answerTextService;
        this.answerPieChartService = answerPieChartService;
        this.answerColChartService = answerColChartService;
        this.userService = userService;
    }

    int limit = 11;


    public User getUser() {
        CustomUserDetail customUserDetail = (CustomUserDetail) (SecurityContextHolder.getContext()).getAuthentication().getPrincipal();
        //   User user2 = userService.findByUsername(customUserDetail.getUser().getUsername());
        return customUserDetail.getUser();
    }

    //danh sách đầu tiên khi chạy trang(sắp xếp theo id giảm dần)
    @GetMapping("/forms/{page}")
    public ResponseEntity<List<Form>> getForms(@PathVariable("page") int page) {
        System.out.println(getUser());
        List<Form> forms = formService.getAllForm(getUser().getId(), page, limit);
        if (forms.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(forms, HttpStatus.OK);
    }

    //danh sách sắp xếp title A - Z
    @GetMapping("/forms/ascending/{page}")
    public ResponseEntity<List<Form>> getFormsAZ(@PathVariable("page") int page) {
        List<Form> forms = formService.getAllFormAZ(getUser().getId(), page, limit);
        if (forms.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(forms, HttpStatus.OK);
    }

    //danh sách sắp xếp title Z - A
    @GetMapping("/forms/descending/{page}")
    public ResponseEntity<List<Form>> getFormsZA(@PathVariable("page") int page) {
        List<Form> forms = formService.getAllFormZA(getUser().getId(), page, limit);
        if (forms.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(forms, HttpStatus.OK);
    }

    // cập nhập title cho biểu mẫu
    @PutMapping("/forms/{id}")
    public ResponseEntity<Form> updateForm(@PathVariable("id") Integer id, @RequestBody Form form) {
        Optional<Form> f = formService.findByIdForm(id);
        if (!f.isPresent()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        f.get().setTitle(form.getTitle());
        formService.saveForm(f.get());
        return new ResponseEntity<>(f.get(), HttpStatus.OK);
    }

    //xóa biểu mẫu
    @DeleteMapping("/forms/{id}")
    public ResponseEntity<Form> deleteForm(@PathVariable("id") Integer id) {
        Optional<Form> f = formService.findByIdForm(id);
        if (!f.isPresent()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } else {
            List<Part> partList = partService.getlistpart(f.get().getId());
            if (partList.isEmpty()) {
                formService.removeForm(f.get());
                return new ResponseEntity<>(HttpStatus.OK);
            } else {
                for (int i = 0; i < partList.size(); i++) {
                    List<Questions> listQuestions = questionService.listQuestionbyIdpart(partList.get(i).getId());
                    if (!listQuestions.isEmpty()) {
                        for (int j = 0; j < listQuestions.size(); j++) {
                            ;
                            Integer idQ = listQuestions.get(j).getId();
                            questionDetailService.deleteQDetail(idQ);
                            questionDetailGridService.deleteQDetailGrid(idQ);
                            Optional<Answers> answersList = answersService.selectByIdQuestion(idQ);
                            if (answersList.isPresent()) {
                                Integer idA = answersList.get().getId();
                                answerTextService.deleteAswerText(idA);
                                answerPieChartService.deleteAswerPieChart(idA);
                                answerColChartService.deletetCol(idA);
                                answersService.deletetblAnswers(idQ);
                            }
                        }
                        questionService.removeQ(partList.get(i).getId());
                    }
                }
                partService.deletePartbyIdForm(f.get().getId());
                formService.removeForm(f.get());
                return new ResponseEntity<>(HttpStatus.OK);
            }
        }
    }

    //phân trang
    @GetMapping("/forms/numberpage")
    public Integer numberPage(@RequestParam(required = false) String title) {
        if (title == null) title = "";
        Integer idU = getUser().getId();
        return formService.countFormsbyTitle(title, idU) % limit == 0 ? formService.countFormsbyTitle(title, idU) / limit : (formService.countFormsbyTitle(title, idU) / limit + 1);
    }

    //get form by id
    @GetMapping("/forms/api/{id}")
    public ResponseEntity<Form> getFormById(@PathVariable("id") int id) {
        Optional<Form> form = formService.findByIdForm(id);
        if (!form.isPresent()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(form.get(), HttpStatus.OK);
    }

    @GetMapping("/forms/APIEdit/{id}")
    public ResponseEntity<Form> getFormByIduser(@PathVariable("id") int id) {
        Form form = formService.findbyIdFormUser(id, getUser().getId());
        if (form == null) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(form, HttpStatus.OK);
    }

    //cập nhật status cho biểu mẫu
    @PutMapping("/forms/status/{id}")
    public ResponseEntity<Form> updateStatusForm(@PathVariable("id") Integer id) {
        Optional<Form> f = formService.findByIdForm(id);
        if (!f.isPresent()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        if (f.get().getStatus()) {
            f.get().setStatus(false);
        } else {
            f.get().setStatus(true);
        }
        formService.saveForm(f.get());
        return new ResponseEntity<>(f.get(), HttpStatus.OK);
    }

    @GetMapping("/forms/lastId")
    public ResponseEntity<Integer> getIdLastForm() {
        Optional<Integer> id = formService.idLastForm();
        if (!id.isPresent()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(id.get(), HttpStatus.OK);
    }

    @PostMapping("/forms/insert")
    public ResponseEntity<Form> insertForm(@RequestBody Form form) {
        //set status default true
        form.setStatus(true);
        //set date now
        form.setDateCreate(new Date(Calendar.getInstance().getTime().getTime()));
        //set user form
        System.out.println(getUser());
        form.setIdUser(getUser().getId());

        formService.saveForm(form);

        return new ResponseEntity<>(form, HttpStatus.CREATED);
    }

    @GetMapping("/forms/title")
    public ResponseEntity<List<Form>> searchFormbyTitle(@RequestParam(required = false) String title, @RequestParam(required = false) Integer page) {
        if (title == null) title = "";
        if (page == null) page = 0;
        List<Form> f = formService.finfAllFormbyTitle(title, getUser().getId(), page, limit);
        if (f.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(f, HttpStatus.OK);
    }

    //
    @GetMapping(value = "/info/user")
    public ResponseEntity<User> getAvartar() {
        if(getUser() == null){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(getUser(),HttpStatus.OK);
    }

    @PutMapping(value = "/change/newavartar")
    public ResponseEntity<User> update(@RequestBody User user) {
        getUser().setAvatar(user.getAvatar());
        userService.save(getUser());
        return new ResponseEntity<>(getUser(),HttpStatus.OK);
    }

    @PutMapping("/form/countanswer/{id}")
    public ResponseEntity<Form> countAnswer(@PathVariable("id") Integer id){
        Optional<Form> f = formService.findByIdForm(id);
        if (!f.isPresent()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        f.get().setCountAnswers(f.get().getCountAnswers()+1);
        formService.saveForm(f.get());
        return new ResponseEntity<>(f.get(),HttpStatus.OK);
    }

    @DeleteMapping("/forms/deleteanswer/{id}")
    public ResponseEntity<Form> deleteAllAnswerbyIdForm(@PathVariable("id") Integer id) {
        Optional<Form> f = formService.findByIdForm(id);
        if (!f.isPresent()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } else {
            List<Part> partList = partService.getlistpart(f.get().getId());
            if (partList.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.OK);
            } else {
                for (int i = 0; i < partList.size(); i++) {
                    List<Questions> listQuestions = questionService.listQuestionbyIdpart(partList.get(i).getId());
                    if (!listQuestions.isEmpty()) {
                        for (int j = 0; j < listQuestions.size(); j++) {
                            Integer idQ = listQuestions.get(j).getId();
                            Optional<Answers> answersList = answersService.selectByIdQuestion(idQ);
                            if (answersList.isPresent()) {
                                Integer idA = answersList.get().getId();
                                answerTextService.deleteAswerText(idA);
                                answerPieChartService.deleteAswerPieChart(idA);
                                answerColChartService.deletetCol(idA);
                            }
                        }
                    }
                }
                f.get().setCountAnswers(0);
                formService.saveForm(f.get());
                return new ResponseEntity<>(HttpStatus.OK);
            }
        }
    }
}
