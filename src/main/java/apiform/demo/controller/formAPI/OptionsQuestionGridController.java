package apiform.demo.controller.formAPI;

import apiform.demo.entity.answersEntity.Answers;
import apiform.demo.entity.questionEntity.QuestionDetailGrid;
import apiform.demo.service.answersService.AnswerColChartService;
import apiform.demo.service.answersService.AnswersService;
import apiform.demo.service.questionservice.QuestionDetailGridService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(path = "/formAPI")
public class OptionsQuestionGridController {
    private QuestionDetailGridService questionDetailGridService;
    private AnswerColChartService answerColChartService;
    private AnswersService answersService;

    @Autowired
    public OptionsQuestionGridController(QuestionDetailGridService questionDetailGridService, AnswerColChartService answerColChartService, AnswersService answersService) {
        this.questionDetailGridService = questionDetailGridService;
        this.answerColChartService = answerColChartService;
        this.answersService = answersService;
    }

    @GetMapping("/forms/part/questions/optionsGrid/{id}")
    public ResponseEntity<List<QuestionDetailGrid>> getOptionsGridByQuestionId(@PathVariable("id") Integer id){
        List<QuestionDetailGrid> questionDetailGrids = questionDetailGridService.getAllByQuestionId(id);
        if(questionDetailGrids.isEmpty()){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(questionDetailGrids, HttpStatus.OK);
    }

    @GetMapping("/forms/questions/optionsGrid/rows/{id}")
    public ResponseEntity<List<QuestionDetailGrid>> getRowsByQuestionId(@PathVariable("id") Integer id) {
        List<QuestionDetailGrid> rows = questionDetailGridService.getRowsByQuestionId(id);
        if (rows.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(rows, HttpStatus.OK);
    }

    @GetMapping("/forms/questions/optionsGrid/cols/{id}")
    public ResponseEntity<List<QuestionDetailGrid>> getColsByQuestionId(@PathVariable("id") Integer id) {
        List<QuestionDetailGrid> cols = questionDetailGridService.getColsByQuestionId(id);
        if (cols.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(cols, HttpStatus.OK);
    }

    @PostMapping("/forms/part/questions/optionsGrid/insert")
    public ResponseEntity<QuestionDetailGrid> insertOptionGridByQuestion(@RequestBody QuestionDetailGrid questionDetailGrid){
        questionDetailGridService.saveOptionGridByQuestion(questionDetailGrid);
        return new ResponseEntity<>(questionDetailGrid, HttpStatus.CREATED);
    }

    @PutMapping("/forms/rowOption/delete/{id}")
    public ResponseEntity<QuestionDetailGrid> deleteRowsByQuestionId(@PathVariable(name = "id") Integer id) {
        Optional<QuestionDetailGrid> questionDetailGrid = questionDetailGridService.findById(id);
        if (!questionDetailGrid.isPresent()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        Optional<Answers> answers = answersService.selectByIdQuestion(questionDetailGrid.get().getIdQuestion());
        answerColChartService.deleteRowsColChart(questionDetailGrid.get().getRows(),answers.get().getId());
        questionDetailGrid.get().setRows("-1");
        questionDetailGridService.saveOptions(questionDetailGrid.get());
        if (questionDetailGrid.get().getCols().equals("-1") && questionDetailGrid.get().getRows().equals("-1")) {
            questionDetailGridService.delteAllOption(id);
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PutMapping("/forms/colOption/delete/{id}")
    public ResponseEntity<QuestionDetailGrid> deleteColsByQuestionId(@PathVariable(name = "id") Integer id) {
        Optional<QuestionDetailGrid> questionDetailGrid = questionDetailGridService.findById(id);
        if (!questionDetailGrid.isPresent()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        Optional<Answers> answers = answersService.selectByIdQuestion(questionDetailGrid.get().getIdQuestion());
        answerColChartService.deleteColsColChart(questionDetailGrid.get().getCols(),answers.get().getId());
        questionDetailGrid.get().setCols("-1");
        questionDetailGridService.saveOptions(questionDetailGrid.get());
        if (questionDetailGrid.get().getCols().equals("-1") && questionDetailGrid.get().getRows().equals("-1")) {
            questionDetailGridService.delteAllOption(id);
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
