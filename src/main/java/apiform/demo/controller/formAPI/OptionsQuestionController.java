package apiform.demo.controller.formAPI;

import apiform.demo.entity.answersEntity.Answers;
import apiform.demo.entity.questionEntity.QuestionDetail;
import apiform.demo.service.answersService.AnswerPieChartService;
import apiform.demo.service.answersService.AnswersService;
import apiform.demo.service.questionservice.QuestionDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(path = "/formAPI")
public class OptionsQuestionController {
    private QuestionDetailService questionDetailService;
    private AnswerPieChartService answerPieChartService;
    private AnswersService answersService;

    @Autowired
    public OptionsQuestionController(QuestionDetailService questionDetailService, AnswerPieChartService answerPieChartService, AnswersService answersService) {
        this.questionDetailService = questionDetailService;
        this.answerPieChartService = answerPieChartService;
        this.answersService = answersService;
    }

    @GetMapping("/forms/part/questions/options/{id}")
    public ResponseEntity<List<QuestionDetail>> getOptionsByQuestionId(@PathVariable("id") Integer id){
        List<QuestionDetail> questionDetails = questionDetailService.getAllByQuestionId(id);
        if(questionDetails.isEmpty()){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(questionDetails, HttpStatus.OK);
    }
    @PostMapping("/forms/part/questions/options/insert")
    public ResponseEntity<QuestionDetail> insertOptionsbyQuestion(@RequestBody QuestionDetail questionDetail){
        questionDetailService.saveOptionsByQuestion(questionDetail);
        return new ResponseEntity<>(questionDetail, HttpStatus.CREATED);
    }

    @DeleteMapping("/forms/option/delete/{id}")
    public ResponseEntity<QuestionDetail> deleteOption(@PathVariable("id") Integer id) {
        Optional<QuestionDetail> questionDetail = questionDetailService.getById(id);
        Optional<Answers> answers = answersService.selectByIdQuestion(questionDetail.get().getIdQuestion());
        answerPieChartService.deleteAswerPieChart(answers.get().getId());
        questionDetailService.deleteOptions(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
