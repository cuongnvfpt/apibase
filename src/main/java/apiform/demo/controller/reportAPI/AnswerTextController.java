package apiform.demo.controller.reportAPI;

import apiform.demo.entity.answersEntity.AnswersText;
import apiform.demo.model.ObjectAnswerText;
import apiform.demo.processor.ConvertStringtoDate;
import apiform.demo.service.answersService.AnswerTextService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/formAPI/report")
public class AnswerTextController extends ConvertStringtoDate {
    private AnswerTextService answerTextService;

    @Autowired
    public AnswerTextController(AnswerTextService answerTextService) {
        this.answerTextService = answerTextService;
    }

    @PostMapping("/answertext")
    public ResponseEntity<AnswersText> insertAswerText(@RequestBody AnswersText answersText){
        answerTextService.save(answersText);
        return new ResponseEntity<>(answersText, HttpStatus.CREATED);
    }

    @GetMapping("/answertext/{idanswer}")
    public ResponseEntity<List<ObjectAnswerText>> selectAswerTextbyIdA(@PathVariable("idanswer") Integer idanswer){
        List<ObjectAnswerText> answersTexts = answerTextService.selectbyIdA(idanswer);
        if(answersTexts.isEmpty()){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(answersTexts, HttpStatus.CREATED);
    }

}
