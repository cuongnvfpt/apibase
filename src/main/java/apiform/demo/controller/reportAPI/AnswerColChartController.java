package apiform.demo.controller.reportAPI;

import apiform.demo.entity.answersEntity.AnswerColChart;
import apiform.demo.model.ObjectAnswerCol;
import apiform.demo.service.answersService.AnswerColChartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(path = "/formAPI/report")
public class AnswerColChartController {
    private AnswerColChartService answerColChartService;

    @Autowired
    public AnswerColChartController(AnswerColChartService answerColChartService) {
        this.answerColChartService = answerColChartService;
    }

    @GetMapping("/answer/colchart/{id}")
    public ResponseEntity<ObjectAnswerCol> tests(@PathVariable("id") Integer id){
        ObjectAnswerCol objectAnswerCol= answerColChartService.getOb(id);
        if(objectAnswerCol == null){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(objectAnswerCol, HttpStatus.OK);
    }
    @PostMapping("/answerColChart")
    public ResponseEntity<AnswerColChart> insertColChart(@RequestBody AnswerColChart colChart) {
        answerColChartService.save(colChart);
        return new ResponseEntity<>(colChart, HttpStatus.CREATED);
    }

    @GetMapping("/answer/getIdByForm/{idForm}")
    public ResponseEntity<List<Integer>> listAnswerId(@PathVariable(name = "idForm") Integer idForm) {
        List<Integer> list = answerColChartService.getAnswerIdByForm(idForm);
        if (list.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(list, HttpStatus.OK);
    }

    @PutMapping("/answer/setCount/{row}/{col}/{idA}")
    public ResponseEntity<AnswerColChart> getCount(@PathVariable(name = "row") String row, @PathVariable(name = "col") String col,@PathVariable(name = "idA") Integer idA) {
        Optional<AnswerColChart> answerColChart = answerColChartService.getAnswerColChart(row, col,idA);
        if (!answerColChart.isPresent()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        Integer newCount = answerColChart.get().getCount() + 1;
        answerColChart.get().setCount(newCount);
        answerColChartService.save(answerColChart.get());
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
