package apiform.demo.controller.reportAPI;

import apiform.demo.entity.answersEntity.AnswerPieChart;
import apiform.demo.model.ObjectAnswerPieChart;
import apiform.demo.service.answersService.AnswerPieChartService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/formAPI/report")
public class AnswerPieChartController {
    private AnswerPieChartService answerPieChartService;

    public AnswerPieChartController(AnswerPieChartService answerPieChartService) {
        this.answerPieChartService = answerPieChartService;
    }
    @PostMapping("/answerpiechart")
    public ResponseEntity<AnswerPieChart> insertAswerText(@RequestBody AnswerPieChart answers){
        answerPieChartService.save(answers);
        return new ResponseEntity<>(answers, HttpStatus.CREATED);
    }
    @GetMapping("/answerpiechart/{idanswer}")
    public ResponseEntity<List<ObjectAnswerPieChart>> selectAswerPCbyIdA(@PathVariable("idanswer") Integer idanswer){
        List<ObjectAnswerPieChart> answersTexts = answerPieChartService.listAnswerPieChart(idanswer);
        if(answersTexts.isEmpty()){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(answersTexts, HttpStatus.CREATED);
    }
}
