package apiform.demo.controller.reportAPI;

import apiform.demo.entity.answersEntity.Answers;
import apiform.demo.service.answersService.AnswersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(path = "/formAPI/report")
public class AnswersController  {
    private AnswersService answersService;

    @Autowired
    public AnswersController(AnswersService answersService) {
        this.answersService = answersService;
    }

    //thêm phần trả lời của người dùng
    @PostMapping("/answers")
    public ResponseEntity<Answers> selecttAnswersByIdQ(@RequestBody Answers Answers){
        answersService.save(Answers);
        return new ResponseEntity<>(Answers, HttpStatus.CREATED);
    }
    @GetMapping("/answers/{idform}")
    public ResponseEntity<List<Answers>> selecttAnswersByIdForm(@PathVariable("idform") Integer idform){
        List<Answers> answers = answersService.selectByIdForm(idform);
        if(answers.isEmpty()){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(answers, HttpStatus.CREATED);
    }

    // get answers by question
    @GetMapping("/answers/get/{idQ}")
    public ResponseEntity<Answers> getAnswersByQuestion(@PathVariable("idQ") Integer idQ){
        Optional<Answers> answers = answersService.selectByIdQuestion(idQ);
        if(!answers.isPresent()){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(answers.get(), HttpStatus.OK);
    }
}
