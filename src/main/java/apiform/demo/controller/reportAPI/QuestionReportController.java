package apiform.demo.controller.reportAPI;

import apiform.demo.entity.questionEntity.Questions;
import apiform.demo.service.questionservice.QuestionService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(path = "/formAPI/report")
public class QuestionReportController {
    private QuestionService questionService;
    
    public QuestionReportController(QuestionService questionService) {
        this.questionService = questionService;
    }

    @GetMapping("/questionid/{id}")
    public ResponseEntity<Questions> getQuestionById(@PathVariable("id")Integer id){
        Optional<Questions> q = questionService.findQbyId(id);
        if(!q.isPresent()){
            return new ResponseEntity<>( HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(q.get(), HttpStatus.OK);
    }

    @GetMapping("/questionform/{idform}")
    public ResponseEntity<List<Questions>> getQByIdForm(@PathVariable("idform") Integer idform){
        List<Questions> listQ = questionService.findQbyIdForm(idform);
        if(listQ.isEmpty()){
            return new ResponseEntity<>( HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(listQ, HttpStatus.OK);
    }
}
