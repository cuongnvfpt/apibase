package apiform.demo.controller.addressAPI;

import apiform.demo.entity.addressEntity.City;
import apiform.demo.service.addressService.CityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(path = "/formAPI/address")
public class CityController {
    @Autowired
    CityService cityService;

    @RequestMapping(value = "/city", method = RequestMethod.GET)
    public List<City> getListCity(){
        return cityService.listCity();
    }

}
