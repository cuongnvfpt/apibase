package apiform.demo.controller.addressAPI;

import apiform.demo.entity.addressEntity.District;
import apiform.demo.service.addressService.DistrictService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(path = "/formAPI/address")
public class DistrictController {
    @Autowired
    DistrictService districtService;

    @RequestMapping(value = "/district/{id}", method = RequestMethod.GET)
    public List<District> getListCity(@PathVariable(name = "id") String id){
        return districtService.getListDistrict(id);
    }
}
