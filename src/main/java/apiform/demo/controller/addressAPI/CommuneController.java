package apiform.demo.controller.addressAPI;

import apiform.demo.entity.addressEntity.Commune;
import apiform.demo.service.addressService.CommuneService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(path = "/formAPI/address")
public class CommuneController {
    @Autowired
    CommuneService communeService;
    @RequestMapping(value = "/commune/{id}", method = RequestMethod.GET)
    public List<Commune> getListCommune(@PathVariable(name = "id") String id ){
        return communeService.getListCommune(id);
    }

}
