package apiform.demo.controller.loginAPI;



import apiform.demo.entity.loginEntity.User;
import apiform.demo.entity.payload.LoginRequest;
import apiform.demo.entity.payload.LoginResponse;
import apiform.demo.model.UserDTO;
import apiform.demo.repository.loginRepository.UserRepository;
import apiform.demo.security.JwtTokenProvider;
import apiform.demo.service.loginService.CustomUserDetail;
import apiform.demo.service.loginService.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping("/loginAPI")
public class loginAPI {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserService userService;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    private JwtTokenProvider tokenProvider;

    @PostMapping(value = "/authenticate")
    public LoginResponse authenticateUser(@Valid @RequestBody LoginRequest loginRequest) throws AuthenticationException {
        // Xác thực từ username và password.
        final Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        loginRequest.getUsername(),
                        loginRequest.getPassword()
                )
        );
        // Nếu không xảy ra exception tức là thông tin hợp lệ
        // Set thông tin authentication vào Security Context
        SecurityContextHolder.getContext().setAuthentication(authentication);
        // Trả về jwt cho người dùng.
        String jwt = tokenProvider.generateToken(authentication);
        return new LoginResponse(jwt);
    }

    @PutMapping(value = "change/newpassword")
    public boolean updatepassword(@RequestBody User user, HttpServletRequest request) {
        CustomUserDetail customUserDetail = (CustomUserDetail) (SecurityContextHolder.getContext()).getAuthentication().getPrincipal();
        User user2 = userService.findByUsername(customUserDetail.getUser().getUsername());

        if (user2 != null) {
            user2.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
            user2.setFirstlogin(true);
            userService.save(user2);
            return true;
        } else {
            return false;
        }
    }


    @PostMapping(value = "web/testpassword/{username}")
    public Boolean testPassword(@PathVariable("username") String username, HttpServletRequest request) {
        User user = userService.findByUsername(username);
        if (user != null) {
            if (user.getFirstlogin() == false) {
                return false;
            }
            if (user.getFirstlogin() == true) {
                return true;
            }
        } else {
            
            return null;
        }
        return null;
    }

    @PutMapping(value = "web/resetpassword/{token}")
    public boolean setNewPassword(@RequestBody User user,@PathVariable("token") String token, HttpServletRequest request) {
        Optional<User> optional = userService.findByResettocken(token);
        User user1 = optional.get();
      //  User user1 = userService.findByUsername(userTocken.getUsername());
        if (user1 != null) {
            user1.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
            user1.setResettocken(null);
            userService.save(user1);
            return true;
        } else {
            return false;
        }
    }
    @PostMapping(value = "web/checkTocken")
    public String checkTocken(@RequestBody UserDTO userDTO, HttpServletRequest request) {
        Optional<User> optional = userService.findByResettocken(userDTO.getResettocken());
        if (optional.isPresent()) {
            return userDTO.getResettocken();
        } else {
            return "false";
        }
    }


}
