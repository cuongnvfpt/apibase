package apiform.demo.entity.formEntity;

import javax.persistence.*;
import java.sql.Date;

@Entity
@Table(name = "tbl_form")
public class Form {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;
    @Column(name = "title")
    private String title;
    @Column(name = "shortd")
    private String shortD;
    @Column(name = "datecreate")
    private Date dateCreate;
    @Column(name = "status")
    private Boolean status;
    @Column(name = "image")
    private String image;
    @Column(name = "iduser")
    private int idUser;
    @Column(name = "countanswers")
    private int countAnswers;


    public int getCountAnswers() {
        return countAnswers;
    }

    public void setCountAnswers(int countAnswers) {
        this.countAnswers = countAnswers;
    }

    public Form() {
    }

    public Form(int id, String title, String shortD, Date dateCreate, int idUser) {
        this.id = id;
        this.title = title;
        this.shortD = shortD;
        this.dateCreate = dateCreate;
        this.idUser = idUser;
    }

    public Form(int id, String title, String shortD, Date dateCreate, Boolean status, String image, int idUser) {
        this.id = id;
        this.title = title;
        this.shortD = shortD;
        this.dateCreate = dateCreate;
        this.status = status;
        this.image = image;
        this.idUser = idUser;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getShortD() {
        return shortD;
    }

    public void setShortD(String shortD) {
        this.shortD = shortD;
    }

    public Date getDateCreate() {
        return dateCreate;
    }

    public void setDateCreate(Date dateCreate) {
        this.dateCreate = dateCreate;
    }

    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }
}
