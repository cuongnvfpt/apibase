package apiform.demo.entity.partEntity;

import javax.persistence.*;

@Entity
@Table(name = "tbl_part")
public class Part {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;
    @Column(name = "title")
    private String title;
    @Column(name = "shortd")
    private String shortD;
    @Column(name = "idform")
    private int idForm;

    public Part() {
    }

    public Part(int id, String title, String shortD, int idForm) {
        this.id = id;
        this.title = title;
        this.shortD = shortD;
        this.idForm = idForm;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getShortD() {
        return shortD;
    }

    public void setShortD(String shortD) {
        this.shortD = shortD;
    }

    public int getIdForm() {
        return idForm;
    }

    public void setIdForm(int idForm) {
        this.idForm = idForm;
    }
}
