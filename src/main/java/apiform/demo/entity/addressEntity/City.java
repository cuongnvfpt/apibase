package apiform.demo.entity.addressEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tbl_address")
public class City {

    @Id
    @Column(name = "idcity")
    private String idCity;

    @Column(name = "city")
    private String city;

    public City() {
    }

    public City(String idCity, String city) {
        this.idCity = idCity;
        this.city = city;
    }

    public String getIdCity() {
        return idCity;
    }

    public void setIdCity(String idCity) {
        this.idCity = idCity;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
}
