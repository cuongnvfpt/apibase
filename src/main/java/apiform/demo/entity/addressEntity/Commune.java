package apiform.demo.entity.addressEntity;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tbl_address")
public class Commune {
    @Id
    @Column(name = "idpx")
    private String idPx;
    @Column(name = "commune")
    private String communeName;
    @Column(name = "idqh")
    private String idQh;

    public Commune() {
    }

    public Commune(String idPx, String communeName, String idQh) {
        this.idPx = idPx;
        this.communeName = communeName;
        this.idQh = idQh;
    }

    public String getIdPx() {
        return idPx;
    }

    public void setIdPx(String idPx) {
        this.idPx = idPx;
    }

    public String getCommuneName() {
        return communeName;
    }

    public void setCommuneName(String communeName) {
        this.communeName = communeName;
    }

    public String getIdQh() {
        return idQh;
    }

    public void setIdQh(String idQh) {
        this.idQh = idQh;
    }
}
