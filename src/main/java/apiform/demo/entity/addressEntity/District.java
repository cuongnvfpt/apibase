package apiform.demo.entity.addressEntity;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tbl_address")
public class District {
    @Id
    @Column(name = "idqh")
    private String idDistrict;

    @Column(name = "district")
    private String districtName;

    @Column(name = "idcity")
    private String idCity;

    public District() {
    }

    public District(String idDistrict, String districtName,String idCity) {
        this.idDistrict = idDistrict;
        this.districtName = districtName;
        this.idCity = idCity;
    }

    public String getIdCity() {
        return idCity;
    }

    public void setIdCity(String idCity) {
        this.idCity = idCity;
    }

    public String getIdDistrict() {
        return idDistrict;
    }

    public void setIdDistrict(String idDistrict) {
        this.idDistrict = idDistrict;
    }

    public String getDistrictName() {
        return districtName;
    }

    public void setDistrictName(String districtName) {
        this.districtName = districtName;
    }
}
