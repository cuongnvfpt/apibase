package apiform.demo.entity.answersEntity;

import javax.persistence.*;

@Entity
@Table(name = "tbl_typeshow")
public class TypeShow {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;
    @Column(name = "nametype")
    private String nametype;

    public TypeShow() {
    }

    public TypeShow(Integer id, String nametype) {
        this.id = id;
        this.nametype = nametype;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNametype() {
        return nametype;
    }

    public void setNametype(String nametype) {
        this.nametype = nametype;
    }
}
