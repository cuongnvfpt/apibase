package apiform.demo.entity.answersEntity;

import javax.persistence.*;

@Entity
@Table(name = "tbl_answerstext")
public class AnswersText {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;
    @Column(name = "answer")
    private String answer;
    @Column(name = "idanswer")
    private Integer idanswer;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public Integer getIdanswer() {
        return idanswer;
    }

    public void setIdanswer(Integer idanswer) {
        this.idanswer = idanswer;
    }

    public AnswersText() {
    }

    public AnswersText(Integer id, String answer, Integer idanswer) {
        this.id = id;
        this.answer = answer;
        this.idanswer = idanswer;
    }
}
