package apiform.demo.entity.answersEntity;

import javax.persistence.*;

@Entity
@Table(name = "tbl_answercolchart")
public class AnswerColChart {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;
    @Column(name = "row")
    private String row;
    @Column(name = "col")
    private String col;
    @Column(name = "count")
    private Integer count;
    @Column(name = "idanswer")
    private Integer idAnswer;

    public AnswerColChart() {
    }

    public AnswerColChart(Integer id, String row, String col, Integer count, Integer idAnswer) {
        this.id = id;
        this.row = row;
        this.col = col;
        this.count = count;
        this.idAnswer = idAnswer;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRow() {
        return row;
    }

    public void setRow(String row) {
        this.row = row;
    }

    public String getCol() {
        return col;
    }

    public void setCol(String col) {
        this.col = col;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public Integer getIdAnswer() {
        return idAnswer;
    }

    public void setIdAnswer(Integer idAnswer) {
        this.idAnswer = idAnswer;
    }
}
