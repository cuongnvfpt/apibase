package apiform.demo.entity.answersEntity;

import javax.persistence.*;

@Entity
@Table(name = "tbl_answers")
public class Answers {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;
    @Column(name = "idquestion")
    private Integer idquestion;
    @Column(name = "idtypeshow")
    private Integer idtypeshow;
    @Column(name = "idform")
    private Integer idform;

    public Answers() {
    }

    public Answers(Integer id, Integer idquestionl, Integer idtypeshow, Integer iform) {
        this.id = id;
        this.idquestion = idquestionl;
        this.idtypeshow = idtypeshow;
        this.idform = iform;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIdquestion() {
        return idquestion;
    }

    public void setIdquestion(Integer idquestionl) {
        this.idquestion = idquestionl;
    }

    public Integer getIdtypeshow() {
        return idtypeshow;
    }

    public void setIdtypeshow(Integer idtypeshow) {
        this.idtypeshow = idtypeshow;
    }

    public Integer getIdform() {
        return idform;
    }

    public void setIdform(Integer iform) {
        this.idform = iform;
    }
}
