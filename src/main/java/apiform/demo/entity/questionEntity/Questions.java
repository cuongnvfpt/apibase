package apiform.demo.entity.questionEntity;

import javax.persistence.*;

@Entity
@Table(name = "tbl_questions")
public class Questions {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;
    @Column(name = "question")
    private String question;
    @Column(name = "idtypequestion")
    private int idTypeQuestion;
    @Column(name = "image")
    private String image;
    @Column(name = "required")
    private Boolean required;
    @Column(name = "idpart")
    private Integer idPart;

    public Questions() {
    }

    public Questions(Integer id, String question, String image) {
        this.id = id;
        this.question = question;
        this.image = image;
    }

    public Questions(Integer id, String question, int idTypeQuestion, String image, Boolean required, Integer idPart) {
        this.id = id;
        this.question = question;
        this.idTypeQuestion = idTypeQuestion;
        this.image = image;
        this.required = required;
        this.idPart = idPart;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public int getIdTypeQuestion() {
        return idTypeQuestion;
    }

    public void setIdTypeQuestion(int idTypeQuestion) {
        this.idTypeQuestion = idTypeQuestion;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Integer getIdPart() {
        return idPart;
    }

    public void setIdPart(Integer idPart) {
        this.idPart = idPart;
    }

    public Boolean getRequired() {
        return required;
    }

    public void setRequired(Boolean required) {
        this.required = required;
    }
}
