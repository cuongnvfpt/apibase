package apiform.demo.entity.questionEntity;

import javax.persistence.*;

@Entity
@Table(name = "tbl_questiondetail")
public class QuestionDetail {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;
    @Column(name = "optionques")
    private String optionQuestion;
    @Column(name = "idquestion")
    private Integer idQuestion;

    public QuestionDetail() {
    }

    public QuestionDetail(Integer id, String optionQuestion, Integer idQuestion) {
        this.id = id;
        this.optionQuestion = optionQuestion;
        this.idQuestion = idQuestion;
    }

    public Integer getId() {
        return id;
    }

    public void setId(int Integer) {
        this.id = id;
    }

    public String getOptionQuestion() {
        return optionQuestion;
    }

    public void setOptionQuestion(String optionQuestion) {
        this.optionQuestion = optionQuestion;
    }

    public Integer getIdQuestion() {
        return idQuestion;
    }

    public void setIdQuestion(Integer idQuestion) {
        this.idQuestion = idQuestion;
    }
}
