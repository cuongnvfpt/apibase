package apiform.demo.entity.questionEntity;

import javax.persistence.*;

@Entity
@Table(name = "tbl_typequestion")
public class TypeQuestion {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;
    @Column(name = "name")
    private String name;
    @Column(name = "typeshow")
    private int typeShow;

    public TypeQuestion() {
    }

    public TypeQuestion(int id, String name, int typeShow) {
        this.id = id;
        this.name = name;
        this.typeShow = typeShow;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getTypeShow() {
        return typeShow;
    }

    public void setTypeShow(int typeShow) {
        this.typeShow = typeShow;
    }
}
