package apiform.demo.entity.questionEntity;

import javax.persistence.*;

@Entity
@Table(name = "tbl_questiondetailgrid")
public class QuestionDetailGrid {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;
    @Column(name = "rows")
    private String rows;
    @Column(name = "cols")
    private String cols;
    @Column(name = "idquestion")
    private int idQuestion;

    public QuestionDetailGrid() {
    }

    public QuestionDetailGrid(int id, String rows, String cols, int idQuestion) {
        this.id = id;
        this.rows = rows;
        this.cols = cols;
        this.idQuestion = idQuestion;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRows() {
        return rows;
    }

    public void setRows(String rows) {
        this.rows = rows;
    }

    public String getCols() {
        return cols;
    }

    public void setCols(String cols) {
        this.cols = cols;
    }

    public int getIdQuestion() {
        return idQuestion;
    }

    public void setIdQuestion(int idQuestion) {
        this.idQuestion = idQuestion;
    }
}
