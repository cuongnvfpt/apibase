package apiform.demo.model;


import apiform.demo.entity.answersEntity.AnswerColChart;

import java.util.List;

public class ObjectAnswerCol {
    private List<String> listRow;
    private List<String> listCol;
    private List<AnswerColChart> listColChart;

    public List<String> getListRow() {
        return listRow;
    }

    public void setListRow(List<String> listRow) {
        this.listRow = listRow;
    }

    public List<String> getListCol() {
        return listCol;
    }

    public void setListCol(List<String> listCol) {
        this.listCol = listCol;
    }

    public List<AnswerColChart> getListColChart() {
        return listColChart;
    }

    public void setListColChart(List<AnswerColChart> listColChart) {
        this.listColChart = listColChart;
    }

    public ObjectAnswerCol() {
    }

    public ObjectAnswerCol(List<String> listRow, List<String> listCol, List<AnswerColChart> listColChart) {
        this.listRow = listRow;
        this.listCol = listCol;
        this.listColChart = listColChart;
    }
}
