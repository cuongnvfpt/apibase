package apiform.demo.model;

public class ObjectAnswerText {
    private String answer;
    private Integer countanswer;
    private Integer idanswer;

    public ObjectAnswerText(String answer, Integer countanswer, Integer idanswer) {
        this.answer = answer;
        this.countanswer = countanswer;
        this.idanswer = idanswer;
    }


    public Integer getIdanswer() {
        return idanswer;
    }

    public void setIdanswer(Integer idanswer) {
        this.idanswer = idanswer;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public Integer getCountanswer() {
        return countanswer;
    }

    public void setCountanswer(Integer countanswer) {
        this.countanswer = countanswer;
    }

    public ObjectAnswerText() {
    }

}
