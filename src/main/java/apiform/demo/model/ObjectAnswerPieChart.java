package apiform.demo.model;

public class ObjectAnswerPieChart {
    private String answer;
    private Integer countanswer;

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public Integer getCountanswer() {
        return countanswer;
    }

    public void setCountanswer(Integer countanswer) {
        this.countanswer = countanswer;
    }

    public ObjectAnswerPieChart() {
    }

    public ObjectAnswerPieChart(String answer, Integer countanswer) {
        this.answer = answer;
        this.countanswer = countanswer;
    }
}
