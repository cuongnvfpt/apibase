package apiform.demo.model;

import java.util.List;

public class Test {
    List<ObjectAnswerPieChart> listPie;
    List<ObjectAnswerText> listText;

    public List<ObjectAnswerPieChart> getListPie() {
        return listPie;
    }

    public void setListPie(List<ObjectAnswerPieChart> listPie) {
        this.listPie = listPie;
    }

    public List<ObjectAnswerText> getListText() {
        return listText;
    }

    public void setListText(List<ObjectAnswerText> listText) {
        this.listText = listText;
    }

    public Test() {
    }

    public Test(List<ObjectAnswerPieChart> listPie, List<ObjectAnswerText> listText) {
        this.listPie = listPie;
        this.listText = listText;
    }
}
